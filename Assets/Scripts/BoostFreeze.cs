using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BoostFreeze : MonoBehaviour, Boosts
{
    void Update()
    {
        transform.Rotate(Vector3.up * 25 * Time.deltaTime);
    }
    void FreezeBoost()
    {
        if (gameObject.CompareTag("Boost1"))
        {
            
            GameManager._reloadIsGameActive = true;
            GameManager._isGameActive = false;
            Destroy(gameObject);
        }
      
    }
    public void OnMouseDown()
    {
        FreezeBoost();
    }

 
 
}
