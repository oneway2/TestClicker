using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnBoostClick : MonoBehaviour
{
    private void Update()
    {
        transform.Rotate(Vector3.up * 25 * Time.deltaTime);
    }
    void OnMouseDown()
    {
        if (gameObject.CompareTag("Boost"))
        {
            GameObject[] _taggedObjects = GameObject.FindGameObjectsWithTag("Enemies");
            foreach (GameObject _enemyOnScene in _taggedObjects)
            {
                GameManager._updateScoreValue  += 1;
                Destroy(_enemyOnScene);
            }
            Destroy(gameObject);
        }
    }
}
    