using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SocialPlatforms.Impl;
using Unity.VisualScripting;

public class GameManager : MonoBehaviour
{
    [SerializeField] private GameObject[] _arrayEnemies = new GameObject[3];
    [SerializeField] private GameObject[] _arrayBoosts = new GameObject[2];
    [SerializeField] private Button _newGameButton;
    [SerializeField] private float _firstDotRangeSpawn = 3;
    [SerializeField] private float _secondDotRangeSpawn = 4;
    [SerializeField] private float _currentTime = 0;
    [SerializeField] private float _timerTime = 3;
    public float _enemySpeed = 1;
    public float _fullHp = 10;
    public static bool _reloadIsGameActive = false;
    public static bool _isGameActive = true;
    public static int _updateScoreValue;

    // Start is called before the first frame update
    void Start()
    {
        BeginParameters();
        InvokeRepeating("SpawnManager", 1 , Random.Range(_firstDotRangeSpawn,_secondDotRangeSpawn));
        InvokeRepeating("SpawnManagerBoosts", 1, Random.Range(15,25));
         
    }
    // Update is called once per frame
    void Update()
    {
        StartCoroutine();
        StartCoroutine(IncreaseDifficultyCoroutine());
    }
    void BeginParameters()
    {
        _isGameActive = true;
        _reloadIsGameActive = false;
        _currentTime = _timerTime;
    }
  
    private void SpawnManagerBoosts()
    {
        if (_isGameActive == true) 
        { 
        int _arrayBoostsIndex = (Random.Range(0, _arrayBoosts.Length));
        Instantiate(_arrayBoosts[_arrayBoostsIndex], GenerateSpawnPosition(), Quaternion.identity);
        }
    }
    public void SpawnManager()
    {
    if (_isGameActive == true)
    {
            SpawnEnemy();
            if (_updateScoreValue >= 15)
            {
                SpawnEnemy();
            }
        }
    }
    public void SpawnEnemy()
    {
        int _arrayEnemiesIndex = (Random.Range(0, _arrayEnemies.Length));
        Instantiate(_arrayEnemies[_arrayEnemiesIndex], GenerateSpawnPosition(), _arrayEnemies[_arrayEnemiesIndex].transform.rotation);
    }
    //����� ������ ������
    Vector3 GenerateSpawnPosition()
    {
        float _spawnPositionX = Random.Range(-3f, 2.6f);
        float _spawnPositionY = Random.Range(0.450f, 3.6f);
        float _spawnPositionZ = Random.Range(-4.4f, 4.7f);

        Vector3 _spawnPos = new Vector3(_spawnPositionX, _spawnPositionY, _spawnPositionZ);
        return _spawnPos;

    }
    IEnumerator IncreaseDifficultyCoroutine()
    {
        if(_updateScoreValue >= 5 && _fullHp == 10)
        {
            _enemySpeed += 0.5f;
            _fullHp += 5;
            _firstDotRangeSpawn = 2;
            _secondDotRangeSpawn = 3;
        }
        if(_updateScoreValue >= 10 && _fullHp == 15)
        {
            _enemySpeed += 1f;
            _fullHp += 5;
            _firstDotRangeSpawn = 3;
            _secondDotRangeSpawn = 5;
        }
        if (_updateScoreValue >= 15 && _fullHp == 20)
        {
            _fullHp += 5;
            _enemySpeed += 1f;
            _firstDotRangeSpawn = 3;
            _secondDotRangeSpawn = 4;
        }
        if (_updateScoreValue >= 20 && _fullHp == 20)
        {
            _enemySpeed += 1f;
            _fullHp += 5;
            _firstDotRangeSpawn = 2;
            _secondDotRangeSpawn = 4;
        }
        yield break;
    }
    //��� ���������� ���������� ������� �� ��������� ������ � ������� ��������� ������� � ������� ����������
    private void StartCoroutine()
    {
        if (_reloadIsGameActive == true)
        {
            StartCoroutine(startTimer());
        }
    }
    IEnumerator startTimer()
    {
        //Timer for reload
        _currentTime -= 1 * Time.deltaTime;

        if (_currentTime <= 0)
        {
            _isGameActive = true;
            if (_isGameActive == true)
            {
                _currentTime = 3;
            }
            _reloadIsGameActive = false;
        }
        if (_currentTime <= 0)
        {
            yield break;
        }

    }


}
   


